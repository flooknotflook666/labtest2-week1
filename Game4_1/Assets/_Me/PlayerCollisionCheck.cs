﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerCollisionCheck : MonoBehaviour {
  TextMeshPro _textMeshPro = null;
    // Start is called before the first frame update
    void Start () {
      _textMeshPro = this.GetComponentInChildren<TextMeshPro>();
    }

    // Update is called once per frame
    void Update () {

    }
    private void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == "Item") {
            Destroy (collision.gameObject);
        } else if (collision.gameObject.tag == "Obstacle") {
            /*RigidBody rb = collision.gameObject.GetComponent<Rigidbody>();
            rb.AddForce(0,10,0,ForceMode.Impluse);*/
                        Destroy (collision.gameObject, 0.5f);

        }
        
        ItemTypeComponent itc = collision.gameObject.GetComponent<ItemTypeComponent> ();
        if (itc != null) {
            switch (itc.Type) {
                case ItemType.BIGCOIN:
                 _textMeshPro.text = "BIGCOIN";
                break;
                case ItemType.POWERUP:
                 
                  _textMeshPro.text = "PowerUP";
                    
                break;
                case ItemType.COIN:
                 _textMeshPro.text = "COIN";
                break;
                case ItemType.SPHERE_OBSTACLE:
                 _textMeshPro.text = "PowerUP";
                break;
                case ItemType.CYLINDER_OBSTACLE:
                 _textMeshPro.text = "PowerUP";
                break;
                 case ItemType.POWERDOWN:
                  _textMeshPro.text = "PowerDOWN";
                break;
                 case ItemType.BOXITEM:
                  _textMeshPro.text = "BOXITEM";
                break;


            }
        }
    }

}
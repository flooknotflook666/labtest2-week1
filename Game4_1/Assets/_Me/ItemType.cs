﻿public enum ItemType
{

    COIN,

    BIGCOIN,

    POWERUP,

    POWERDOWN,
    BOXITEM,

    SPHERE_OBSTACLE,

    CYLINDER_OBSTACLE,

}
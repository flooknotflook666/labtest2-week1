﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypeComponent : MonoBehaviour
{
    [SerializeField]
    // Start is called before the first frame update
    protected ItemType _itemType;
    public ItemType Type 
    {
        get 
        {
            return _itemType;
        }

        set
        {
             _itemType = value;
        
        }


    }

    protected int _locationIndex;
    public int LocationIndex
    {
        get
        {
              return _locationIndex;
        }
        set{

            _locationIndex = value;
        }
    }


}
    

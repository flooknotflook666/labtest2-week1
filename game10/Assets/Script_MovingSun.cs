﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_MovingSun : MonoBehaviour
{
    [SerializeField]
    float _sunStep = 1;

    void Update(){
        this.gameObject.transform.RotateAround (new Vector3(0,0,0),new Vector3(1,0,0),
        _sunStep*Time.deltaTime);

  
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlObjectMovementOnXZPlaneUsingArrowKeys : MonoBehaviour {
 [SerializeField]
 private float _RotateStep = 0.1f;

 // Use this for initialization
 void Start () {

 }

 // Update is called once per frame
 void Update () {
 //GetKey
 if (Input.GetKey(KeyCode.A))
 {
 this.transform.Rotate(0,- _RotateStep, 0);
 }
 else if (Input.GetKey(KeyCode.D))
 {
 this.transform.Rotate( 0, _RotateStep, 0);
 }
 else if (Input.GetKey(KeyCode.W))
 {
 this.transform.Translate(0, 0, 0);
 }
 else if (Input.GetKey(KeyCode.S))
 {
 this.transform.Translate(0, 0, 0);
 }
 }
 }
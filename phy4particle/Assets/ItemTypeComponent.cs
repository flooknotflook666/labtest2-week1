﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypeComponent : MonoBehaviour
{
    [SerializeField]
    // Start is called before the first frame update
    protected ItemTypes _ItemTypes;
    public ItemTypes Type 
    {
        get 
        {
            return _ItemTypes;
        }

        set
        {
             _ItemTypes = value;
        
        }


    }

    protected int _locationIndex;
    public int LocationIndex
    {
        get
        {
              return _locationIndex;
        }
        set{

            _locationIndex = value;
        }
    }


}
    

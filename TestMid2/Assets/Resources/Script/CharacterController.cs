﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class CharacterController : MonoBehaviour
{
 public PlatformerCharacter2D m_Character;
 public ButtonState _JumpButton;
 public ButtonState _MoveRightButton;
 public ButtonState _MoveLeftButton;
  public bool isPass = false;
  public bool isPassRight = false; 
  public bool isPassLeft = false;
 
 
 private bool m_Jump;
 private float m_Right = 0.3f;
 private float m_Left = -0.3f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update(){
//J
if(_JumpButton._currentState == ButtonState.State.Down &&
 m_Jump == false && isPass == false) {
 m_Jump = true;
 isPass = true;
 }
 else if (_JumpButton._currentState == ButtonState.State.Up &&
 isPass == true) {
 isPass = false;
 }

//R
if(_MoveRightButton._currentState == ButtonState.State.Down &&
 isPassRight == false) {
 
 isPassRight = true;
 }
 else if (_MoveRightButton._currentState == ButtonState.State.Up &&
 isPassRight == true) {
 isPassRight = false;
 }


//L
if(_MoveLeftButton._currentState == ButtonState.State.Down &&
  isPassLeft == false) {
 
 isPassLeft = true;
 }
 else if (_MoveLeftButton._currentState == ButtonState.State.Up &&
isPassLeft == true) {
 isPassLeft = false;
 }




}
 private void FixedUpdate() {
 if(isPass == true){
 m_Character.Move(0, false, m_Jump);
 m_Jump = false;
 }
 
if(isPassRight == true){
 m_Character.Move(m_Right, false, false);
 
}


if(isPassLeft == true){
 m_Character.Move(m_Left, false, false);

}
 
 }

 

}
    
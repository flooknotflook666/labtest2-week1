﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinMovement : MonoBehaviour
{
    [SerializeField]
 private float _angularSpeed = 5.0f;
 [SerializeField]
private Vector3 _axisOfRotation = new Vector3(1.0f,0,0);

 Transform _objTransform;

 // Use this for initialization
 void Start () {
 _objTransform = this.gameObject.GetComponent <Transform >();
 }

// Update is called once per frame
 void Update () {
 _objTransform.Rotate(_axisOfRotation , _angularSpeed);
 }
 }


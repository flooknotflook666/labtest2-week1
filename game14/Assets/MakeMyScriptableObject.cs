﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeMyScriptableObject : MonoBehaviour
{
    [MenuItem("Assets/Create/My Scriptable Object")]
 public static void CreateMyAsset()
 {
 MyScriptableObject asset = ScriptableObject.CreateInstance <MyScriptableObject >();

 AssetDatabase.CreateAsset(asset, "Assets/NewScripableObject.asset");
 AssetDatabase.SaveAssets();

 EditorUtility.FocusProjectWindow();

 Selection.activeObject = asset;
}
}

﻿using UnityEngine;
public class MyScriptableObject : ScriptableObject {
 public new string name = "Name";
 public string description;

 public Sprite artwork;
 public int manacost = 100;
 public int attack = 100;
 public int health = 100;

 public float speed =0.1f;


 }

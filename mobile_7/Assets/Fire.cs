﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour

{
    public int PowerSpeed = 1;
 public float RotationSpeed = 0.5f;
 public GameObject m_Laser;
 Rigidbody2D m_Rigid;
    // Start is called before the first frame update
    void Start()
    {
        m_Rigid = GetComponent<Rigidbody2D> ();
    }

    // Update is called once per frame
    void Update()
    {
        //Fire Laser
 
 GameObject la = Instantiate (m_Laser);
 la.transform.position = this.transform.position;
 la.transform.rotation = this.transform.rotation;
 la.GetComponent<Rigidbody2D> ()
 .AddForce (la.transform.up * 1, ForceMode2D.Impulse);
        
    }
}

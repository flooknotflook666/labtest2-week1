﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseInformation : MonoBehaviour
{
  
    // Start is called before the first frame update
    void Start()
    {
        
    }
public Text _textMousePosition;
public Text _textMouseScrollDelta;
 public Text _textMouseDeltaVector;

 private Vector3 _mousePrevPosition;

    // Update is called once per frame
    void Update()
    {
       Vector3 mouseCurrentPos = Input.mousePosition;
      Vector3 mouseDeltaVector = Vector3.zero;
      mouseDeltaVector = (mouseCurrentPos - _mousePrevPosition).normalized;
       
       
       
       _textMousePosition.text = Input.mousePosition.ToString();
       _textMouseScrollDelta.text = Input.mouseScrollDelta.ToString();
      _textMouseDeltaVector.text = mouseDeltaVector.ToString();

      _mousePrevPosition = mouseCurrentPos;
 
       

    }
}

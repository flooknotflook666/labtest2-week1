﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuControlScript : MonoBehaviour
{
    //Declarations section
 [SerializeField] Button _startButton;
 [SerializeField] Button _startButton2;
 [SerializeField] Button _optionsButton;
 [SerializeField] Button _exitButton;
 //Inside Start() method
 void Start () {
 _startButton.onClick.AddListener (
 delegate{StartButtonClick(_startButton);});
 _startButton2.onClick.AddListener (
 delegate{StartButtonClick2(_startButton2);});
 _optionsButton.onClick.AddListener (
 delegate{OptionsButtonClick(_optionsButton);});
 _exitButton.onClick.AddListener (
 delegate{ExitButtonClick(_exitButton);});
 }

    // Update is called once per frame
    void Update()
    {
        
    }
public void StartButtonClick(Button button) {
  SceneManager.LoadScene("SceneGameplay");
 }
public void StartButtonClick2(Button button) {
  SceneManager.LoadScene("SceneGameplay2");
 }
 public void OptionsButtonClick(Button button) {
 if (!GameApplicationManager.Instance.IsOptionMenuActive)
 {
 SceneManager.LoadScene("SceneOptions", LoadSceneMode.Additive);
 GameApplicationManager.Instance.IsOptionMenuActive = true;
 }
 }

 public void ExitButtonClick(Button button) {
 Application.Quit();
 }
 }

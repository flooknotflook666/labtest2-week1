﻿using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
using UnityEngine.UI;
 using UnityEngine.SceneManagement;

public class OptionsMenuControlScript : MonoBehaviour {
 [SerializeField] Dropdown _dropdownDifficulty;
 [SerializeField] Toggle _toggleMusic;
 [SerializeField] Toggle _toggleSFX;

 // Use this for initialization
 void Start () {
_dropdownDifficulty.value = GameApplicationManager.Instance.DifficultyLevel;
_toggleMusic.isOn = GameApplicationManager.Instance.MusicEnabled;
 _toggleSFX.isOn = GameApplicationManager.Instance.SFXEnabled;

 _toggleMusic.onValueChanged.AddListener(
 delegate {OnToggleMusic(_toggleMusic);});
 _toggleSFX.onValueChanged.AddListener(
 delegate {OnToggleSFX(_toggleSFX);});
 }

 // Update is called once per frame
 void Update () {
 }

 public void BackButtonClick() {
 SceneManager.UnloadSceneAsync("SceneOptions");
 GameApplicationManager.Instance.IsOptionMenuActive = false;
 }

 public void DropdownDifficultyChanged(Dropdown dropdown) {
 GameApplicationManager.Instance.DifficultyLevel = dropdown.value;
 }

 public void OnToggleMusic(Toggle toggle) {
 GameApplicationManager.Instance.MusicEnabled = _toggleMusic.isOn;
 if (GameApplicationManager.Instance.MusicEnabled)
 SoundManager.Instance.MusicVolume = SoundManager.Instance.MusicVolumeDefault;
 else
 SoundManager.Instance.MusicVolume = SoundManager.MUTE_VOLUME;
 }
 public void OnToggleSFX(Toggle toggl)
 {
 GameApplicationManager.Instance.SFXEnabled = _toggleSFX.isOn;
 if (GameApplicationManager.Instance.SFXEnabled)
 SoundManager.Instance.MasterSFXVolume = SoundManager.Instance.
MasterSFXVolumeDefault;
 else
 SoundManager.Instance.MasterSFXVolume = SoundManager.MUTE_VOLUME;
 }
 }
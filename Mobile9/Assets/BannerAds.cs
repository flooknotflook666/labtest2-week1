﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
public class BannerAds : MonoBehaviour
{
 public string bannerPlacement = "Banner";
 public bool testMode = false;
#if UNITY_IOS
 public const string gameID = "3351977";
#elif UNITY_ANDROID
 public const string gameID = "3351976";
#elif UNITY_EDITOR
 public const string gameID = "3351977";
#endif
 void Start()
 {
 Advertisement.Initialize(gameID, testMode);
 StartCoroutine(ShowBannerWhenReady());
 }
 IEnumerator ShowBannerWhenReady()
 {
 while (!Advertisement.IsReady("Banner"))
 {
 yield return new WaitForSeconds(0.5f);
 }
 Advertisement.Banner.Show(bannerPlacement);
 }
}
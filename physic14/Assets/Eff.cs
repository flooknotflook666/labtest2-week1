﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eff : MonoBehaviour


{

    public GameObject _effectObstacleCollision;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnCollisionEnter(Collision other){

        if(other.gameObject.tag == "Enemy"){
            GameObject effect = Instantiate(_effectObstacleCollision,other.gameObject.transform.position,Quaternion.identity);
            Destroy(effect,2.0f);



        }
    }
}
